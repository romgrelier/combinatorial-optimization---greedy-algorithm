from colored import bg, attr


class Building:
    counter = 0

    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.x = 0
        self.y = 0
        self.number = Building.counter
        Building.counter += 1

    def __str__(self):
        output = ""
        for i in range(self.width):
            for j in range(self.height):
                output += "{}{}{:^2}{}".format(attr(0),
                                               bg(self.number), self.number, attr(0))
        return output

    def area(self):
        return self.height * self.width

    def size(self):
        return self.height + self.width
