from city import City
from building import Building


def load(filename):
    """
        H W : size
        N : number of buildings
        x y : building size
    """
    file_content = []

    with open(filename, 'r') as file:
        file_content = file.read().splitlines()

    H, W = list(map(int, file_content[0].split(' ')))
    N = int(file_content[1].split(' ')[0])

    buildings = [Building(int(line.split(' ')[0]), int(
        line.split(' ')[1])) for line in file_content[2:]]

    return City(H, W), buildings
