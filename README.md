# Installer les dépendances 

pip3 install -r requirement.txt

# Lancer le programme

python3 main.py

# Paramètrage

La plupart des paramètres sont dans le fichier main.py, pour des réglages plus fins le fichier  algorithm.py contient les algorithmes utilisés.
