from building import Building
from city import City
from random import randint
from math import sqrt


def generate(L):
    buildings = []

    for _ in range(L):
        buildings.append(
            Building(randint(1, round(2 * sqrt(L))), randint(1, round(2 * sqrt(L)))))

    return City(L, L), buildings
