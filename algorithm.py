from city import City
from copy import deepcopy
from random import shuffle


def fill_row_first(city_base, buildings):
    city = deepcopy(city_base)
    building_list = deepcopy(buildings)

    y = 0
    while(y < city.height):
        x = 0
        while(x < city.width):
            for building in building_list:
                if not city.is_colliding(x, y, building):
                    city.add_building(x, y, building)
                    x += building.width
                    building_list.remove(building)
            x += 1
        y += 1

    return city


def fill_column_first(city_base, buildings):
    city = deepcopy(city_base)
    building_list = deepcopy(buildings)

    x = 0
    while(x < city.width):
        y = 0
        while(y < city.height):
            for building in building_list:
                if not city.is_colliding(x, y, building):
                    city.add_building(x, y, building)
                    y += building.height
                    building_list.remove(building)
            y += 1
        x += 1

    return city


def fill_larger_area_first(city, buildings):
    building_list = sorted(buildings, key=lambda b: b.area(), reverse=True)

    return fill_row_first(city, building_list)


def fill_larger_size_first(city, buildings):
    building_list = sorted(buildings, key=lambda b: b.size(), reverse=True)

    return fill_row_first(city, building_list)


def fill_randomly(city, buildings):
    shuffle(buildings)

    return fill_row_first(city, buildings)


def fill_randomly_many_times(city, buildings, count=1000):
    best = city
    for _ in range(count):
        test = fill_randomly(city, buildings)
        if test.score() > best.score():
            best = test
            print("score = {}".format(best.score()))
            print(best)

    return best
