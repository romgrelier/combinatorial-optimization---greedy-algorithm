from loader import load
from generator import generate
from city import City
import algorithm
from copy import deepcopy

# Choisir entre une instance générée ou l'instance donnée dans le sujet

#city_base, buildings = generate(52)
city_base, buildings = load("data/instance_1")

algorithms = {
    # autre algorithme glouton : remplir les colonnes plutot que les lignes
    "Columns first": algorithm.fill_column_first,
    # premier glouton
    "Rows first": algorithm.fill_row_first,
    # espace occupé
    "Larger area first": algorithm.fill_larger_area_first,
    # encombrement
    "Larger size first": algorithm.fill_larger_size_first,
    # aléatoire
    "Random order": algorithm.fill_randomly,
    # garde le meilleur des aléatoires
    "Many random order": algorithm.fill_randomly_many_times
}

for key, alg in algorithms.items():
    city = alg(city_base, buildings)
    print("{} | score = {} | solution : {}".format(key, city.score(), city.take_solution()))
    print(city)
