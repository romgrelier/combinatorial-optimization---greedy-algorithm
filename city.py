import numpy as np
from colored import bg, attr
from copy import deepcopy


class City:

    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.plan = np.full((height, width), '.', dtype=np.dtype('U4'))
        self.buildings = []

    def __str__(self):
        output = ''
        for r in self.plan:
            for c in r:
                if c != '.':
                    output += "{}{:^4}{}".format(bg(c), c, attr(0))
                else:
                    output += "{}{:^4}".format(attr(0), c)
            output += '\n'
        return output

    def is_colliding(self, x, y, building):
        if x >= 0 and y >= 0 and (x + building.width) <= self.width and (y + building.height) <= self.height:
            for b in self.buildings:
                if x < b.x + b.width and x + building.width > b.x and y < b.y + b.height and y + building.height > b.y:
                    return True
        else:
            return True

        return False

    def add_building(self, x, y, building):
        if not self.is_colliding(x, y, building):
            for i in range(y, y + building.height):
                for j in range(x, x + building.width):
                    self.plan[i][j] = str(building.number)
            building.x = x
            building.y = y
            self.buildings.append(deepcopy(building))

    def take_solution(self):
        output = ""
        for building in self.buildings:
            output += "({}, {}), ".format(building.x, building.y)
        return output

    def score(self):
        total = 0
        for building in self.buildings:
            total += building.area()
        return total
